Monday (optional)

14:00 - 16:00 Intro to Mathematica (Mathematica people?)
16:00 - 16:30 Tea Break
16:30 - 18:00 Reproducible Research (Hermina?)

Tuesday

9:30 - 11:00 Welcome session / Time to introduce ourselves, and to discover each other's expectations for the coming week. A soft introduction to the working environment.
11:00 - 11:30 Coffee Break
11:30 - 12:30 Model design and analysis I (Claudia)
12:30 - 14:00 Lunch Break
14:00 - 16:00 Model design and analysis II / Extract a model from the literature
16:00 - 16:30 Tea Break
16:30 - 18:00 Extract and present a model from the literature / Brainstorming of project ideas

Wednesday

9:30 - 11:00 Morning Wrap-up (what have we done so far?) / Simulations I (Stephan)
11:00 - 11:30 Coffee Break
11:30 - 12:30 Simulations II (Stephan)
12:30 - 14:00 Lunch Break
14:00 - 16:00 Identification of individual projects
16:00 - 16:30 Tea Break
16:30 - 18:00 Assisted work on your project / Pitch your project

Thursday

9:30 - 11:00 Morning Wrap-up (what have we done so far?) / Data fitting/matching/analysis I (?) (Rafael)
11:00 - 11:30 Coffee Break
11:30 - 12:30 Data fitting/matching/analysis II (Rafael)
12:30 - 14:00 Lunch Break
14:00 - 16:00 Assisted work on your project
16:00 - 16:30 Tea Break
16:30 - 18:00 Assisted work on your project

Friday

9:30 - 11:00 Morning Wrap-up (what have we done so far?) / Assisted work on your project
11:00 - 11:30 Coffee Break
11:30 - 12:30 Assisted work on your project
12:30 - 14:00 Lunch Break
14:00 - 16:00 Project presentations
16:00 - 16:30 Tea Break
16:30 - 18:00 Outcomes versus Expectations / Final wrap-up session



